const gulp = require('gulp');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');

// pull in the project TypeScript config
const tsProject = ts.createProject('tsconfig.json');

gulp.task('scripts', () => {
let tsResult = gulp.src(['src/**/*.ts', 'typings/**/*.d.ts'])
    .pipe(sourcemaps.init({ sourceRoot: '.' }))
    .pipe(tsProject());

  return tsResult.js
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist'));
});

gulp.task('scriptsxxx', () => {
  const tsResult = tsProject.src()
  .pipe(tsProject());
  return tsResult.js.pipe(gulp.dest('dist'));
});

gulp.task('resources', () => {
  gulp.src(['node_modules/snowboy/resources/common.res','resources/redalert.pmdl'])
  .pipe(gulp.dest('dist'))

});

gulp.task('watch', ['scripts', 'resources'], () => {
  gulp.watch('src/**/*.ts', ['scripts']);
});

gulp.task('default', ['resources', 'scripts']);


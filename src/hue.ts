import { HueApi, lightState } from 'node-hue-api';

export class Hue {
    
    hueApi : HueApi;

    constructor(private hueEndpoint:string, private username:string) {
        this.hueApi = new HueApi( hueEndpoint, username);
    }


    redAlert( group:number ) {

        //Get Current colours
        console.log("#### redAlert - Group:", group);
      
        var action;
        this.hueApi.group(group).then((lightGroup)=> {
            action = lightGroup["lastAction"];
            console.log("#### Existing Action:", action, "  - Group: ", group);
            return this.hueApi.setGroupLightState(group, 
                lightState.create()
                    .alertLong()
                    .bri(254)
                    .hue(65535)
                    .sat(255)
                    )
        }).then((result) => {
            console.log("#### Waiting for 3 Seconds - Group: ", group);
            return new Promise((resolve) => setTimeout(resolve,5000));
        }).then((result) => {
            console.log("#### Resetting State - Group: ", group);
            return this.hueApi.setGroupLightState(group, 
                lightState.create()
                    .bri(254)
                    .hue(14910)
                    .sat(144)
                    .alert("none")
                    )
        });
    }
}
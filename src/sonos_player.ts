

import { Sonos } from "node-sonos";


export class SonosPlayer {
    sonos:Sonos;
    constructor(private sonosDevice:String) {
        this.sonos = new Sonos(sonosDevice);
    }
    
    playSound(soundUrl:String) {
        var promise = new Promise((resolve,reject)=> {
            this.sonos.queueNext(soundUrl, (x) => {
                if (x) {
                    reject();
                } else {
                    resolve();
                };
            });
        }).then((resolve) => {
            return new Promise((resolve,reject) => {
                this.sonos.play((x)=> {
                    if (x) {
                        reject();
                    } else {
                        resolve();
                    }
                });
            });
        }).catch((err) => {
            console.log("PLAY FAILED ", err);
        })
    };   
}
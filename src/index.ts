const record = require('node-record-lpcm16');
const {Detector, Models} = require('snowboy');

import { SonosPlayer } from "./sonos_player";
import { Hue } from './hue';

const hue = new Hue("10.0.1.3", "w9aDrNKGpCYmQtXrG9Me99LxydA0M6hWVtGddKWd");
const sonosPlayer = new SonosPlayer("10.0.1.196")

var exec = require('child_process').exec

const models = new Models();


models.add({
  file: 'redalert.pmdl',
  sensitivity: '0.5',
  hotwords : 'red alert'
});

const detector = new Detector({
  resource: "common.res",
  models: models,
  audioGain: 5.0
});

detector.on('silence', function () {
  // console.log('silence');
});

detector.on('sound', function () {
  // console.log('sound');

});

detector.on('error', function () {
  console.log('error');
});

detector.on('hotword', function (index, hotword) {
  console.log('hotword', index, hotword);
  
  hue.redAlert(1);
  
  sonosPlayer.playSound("http://trekcore.com/audio/redalertandklaxons/tng_red_alert2.mp3");
});
  
const mic = record.start({
  threshold: 0,
  verbose: true
});

mic.pipe(detector);


